// OMRAG project plan & track
/////// hollerith 2016 ////////////////
 
Session.setDefault('project', 'All');
Session.setDefault('label', 'All');
Session.setDefault('type', 'All');
Session.setDefault('view', 'Complete');
Session.setDefault('parent', null);

PROJECTS = ['All'];
LABELS = ['All', 'Infra', 'ServiceDesk'];
TYPES = ['All', 'Bug','Unicorn'];
VIEWS = ['Default','Estimated','Poker','Sprint'];

Items = new Meteor.Collection('items');
Lists = new Meteor.Collection('lists'); 
Issues = new Meteor.Collection('issues');
Defaults = new Meteor.Collection('defaults');

Meteor.subscribe('items', function () {
    return Items.find();
});

Meteor.subscribe('issues', function () {
    return Issues.find();
});

Meteor.subscribe('lists', function () {
    return Lists.find();
});

Meteor.subscribe('defaults', function () {
    return Defaults.find({ userid: Meteor.userId() });
});

// Bindings
Template.registerHelper(
  'equals', function(v1, v2) {
      return (v1 == v2);
  },
  'notequals', function(v1, v2) {
      return !(v1 == v2);
  }
);

Template.modal.helpers({  
  'activeModal': function() {
    return Session.get('activeModal');
  }
});

Template.message.helpers({  
  'message': function() {
    // Show messages
    message = Session.get('message');
    if (message) {
      if (this._id) {
        message.class = message.class + ' on-white'
      } else {
        message.class = message.class + ' on-graphite'
      }

      if (message.ttl) { Meteor.setTimeout(function(){ Session.set('message', null);}, message.ttl) };
      
      return message;
    }
  }
});

Template.settings.helpers({  
  'currentSettings': function() {
    id = Session.get('currentItem');
    if (id) {
      item = Items.findOne({_id: id });
      return item
    }
  },    
});

Template.addItem.helpers({  
  'newItem': function() {
    item = Items.findOne(Session.get('newItem'));            
    return item
  },    
});

Template.editItem.helpers({  
  'currentItem': function() {
    id = Session.get('currentItem');
    if (id) {
      item = Items.findOne({_id: id });
      return item
    }
  },    
});

Template.header.helpers({
  'project': function (){
    return getList(PROJECTS, 'project');
  },  
  'type': function (){
    return getList(TYPES, 'type');
  },
  'labels': function (){
    return getList(LABELS, 'labels');
  },
  'view': function (){
    return getList(VIEWS, 'view');
  },
  'scoresDoors': function(){
    
    var todo = Session.get("todoCount");
    var test = Session.get("testCount");
    var done = Session.get("doneCount");
    
    var n = 100-parseInt((((todo+test)/(todo+test+done))*100), 10);
    if(n < 0) n *= -1
    return n       
  },
  'todoCount': function(){
    return Session.get("todoCount");
  },
  'testCount': function(){
    return Session.get("testCount");
  },
  'doneCount': function(){
    return Session.get("doneCount");
  },
  'parent': function() {
      return Session.get('parent');
  }        
});

Template.main.helpers({
  'todoItems': function(){
    
    var search = getFilters({ status: "todo"});
    
    todoItems = Items.find(search, { sort: { rank: -1} });      
    Session.set("todoCount", todoItems.count());

    return todoItems;
  },
  'testItems': function(){
    
    var search = getFilters({ status: "test"});
    
    testItems = Items.find(search, { sort: { rank: -1} });
    Session.set("testCount", testItems.count());
    
    return testItems;
  },
  'doneItems': function(){
    
    var search = getFilters({ status: "done"});
    
    doneItems = Items.find(search, { sort: { rank: -1} });  
    Session.set("doneCount", doneItems.count());

    return doneItems
  },  
});

Template.card.helpers({
  'isParent': function(){
    hasChildren = this.childs
    return (hasChildren);
  },
})

// Triggers
Template.header.events({
  'change #project': function (event, template) {
     PROJECTS = Session.get('PROJECTS')
     project = PROJECTS[event.target.selectedIndex]
     Session.set("project", project);
  },
  'change #type': function (event, template) {
      TYPES = Session.get("TYPES");
      type = TYPES[event.target.selectedIndex]
      Session.set("type", type);
  },
  'change #labels': function (event, template) {
      LABELS = Session.get("LABELS");
      label = LABELS[event.target.selectedIndex]
      Session.set("label", label.option);
  },
  'change #view': function (event, template) { 
      VIEWS = Session.get("VIEWS");
      view = VIEWS[event.target.selectedIndex]
      Session.set("view", view);
  },
});

Template.page.events({
  'click .settings': function(event, template) {
    Session.set('activeModal', 'settings');
    $('.modal').fadeIn('fast');
  },
  'click .close': function(event, template) {
    $('.modal').fadeOut('fast');
  },   
  'click .logout': function() {
    Meteor.logout();
  }, 
  'click .card': function(event, template) {

    if ( this.childs == null || this.childs.length == 0 ) {

      initFormWith(this);
      
      Session.set('currentItem', this._id);
      Session.set('activeModal', 'editItem');
      $('.modal').fadeIn('fast');      
    } else {
      
      // subset - check this!
      Session.set('parent', {
          id: this._id
        , title: this.title
        , name: this.name
        , status: this.status
        , ancestors: this.ancestors
        , childs: this.childs
        , parent: this.parent
      });
      
    }
  }, 
  'click .addItem': function(event, template) {
    
    var due = new Date()
      , created = new Date()
      , owner = Meteor.userId()
      , nickname = Meteor.users.findOne(owner);

    var parent = Session.get('parent')
    if (parent) {
      parent = parent.id;
    }
    
    current = Defaults.findOne({ "userid": Meteor.userId() });

    if (!current) {
                  
      current = Defaults.insert({
          "userid": Meteor.userId()
        , "name": 'Premise of the piece.'
        , "title": 'OMS-21121'
        , "description": 'Steps to reproduce.'
        , "project": Session.get('project')
        , "type": 'Template'
        , "status": 'todo'
        , "state": 'todo'
        , "owner": Meteor.user().nickname
        , "users": []
        , "labels": []
        , "comments": []
        , "parent": parent
      });
      
      current = Defaults.findOne({ "_id": current });
    }
        
    var item_id = Items.insert({
        "name": current.name 
      , "title": current.title 
      , "description": current.description 
      , "project": current.project
      , "type": current.type 
      , "status": current.status 
      , "state": current.state 
      , "due": due
      , "created": created
      , "owner": nickname
      , "users": [ nickname ]
      , "labels": []
      , "comments": []
      , "parent": parent
    });

    initFormWith(current);

    Session.set('newItem', item_id);
    Session.set('activeModal', 'addItem');
    $('.modal').fadeIn('fast');            
  },
  'click .editItem': function(event, template) {
    var parent = Session.get('parent')
    if (parent) {

      initFormWith(parent);

      parent = parent.id;
      Session.set('currentItem', parent);
      Session.set('activeModal', 'editItem');
      $('.modal').fadeIn('fast');    
    }
  },
  'click .parent': function(event, template) {
    var parent = Session.get('parent')
    if (parent) {

      initFormWith(parent);

      item_id = parent.id;
      Session.set('currentItem', item_id);
      Session.set('activeModal', 'editItem');
      $('.modal').fadeIn('fast');    
    }
  },
  'mousemove .panel': function(event, template) {
    parent = Session.get('parent');
    if (parent) {
      if (event.pageX < 100) {
        $('.menu').fadeIn('slow');
        Meteor.setTimeout(function(){$('.menu').fadeOut('slow');}, 6000)
      } else {
        $('.menu').fadeOut('slow'); 
      }
    } else {
      $('.menu').fadeOut('fast');       
    }        
  },
  'click .menu': function(event, template) {
    var parent = Session.get('parent')
    if (parent) {
      parent = parent.parent;
      if (parent) {
        self = Items.findOne({ _id: parent })
        Session.set('parent', {
            id: self._id
          , title: self.title
          , name: self.name
          , status: self.status
          , ancestors: self.ancestors
          , childs: self.childs
          , parent: self.parent
        });
      } else {
        Session.set('parent', null);        
      }
    }
  }
});

Template.addItem.events({
  "change #title": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "title":template.find("#title").value }})
  },
  "change #name": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "name":template.find("#name").value }})
  },
  "change #description": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "description":template.find("#description").value }})
  },
  "change #project": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "project":template.find("#project").value }})
  },
  "change #type": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "type":template.find("#type").value }})
  },
  "change #status": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "status":template.find("#status").value }})
  },
  "change #state": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "state":template.find("#state").value }})
  },
  "change #due": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "due":template.find("#due").value }})
  },
  "change #estimate": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "estimate":template.find("#estimate").value }})
  },
  "change #effort": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "effort":template.find("#effort").value }})
  },
  "change #score": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "score":template.find("#score").value }})
  },
  "change #created": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "created":template.find("#created").value }})
  },
  "change #labels": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "labels":template.find("#labels").value }})
  },
  "change #comments": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "comments":template.find("#comments").value }})
  },
  "click .snaphot": function(event, template) {
    
    current = Defaults.findOne({ "userid": Meteor.userId() });
    current = Defaults.update({"_id": current._id}, { $set : {
        "userid": Meteor.userId()
      , "name": this.name
      , "title": this.title
      , "description": this.description
      , "project": this.project
      , "type": this.type
      , "status": this.status
      , "state": this.state
      , "owner": this.owner
      , "users": this.users
      , "labels": this.labels
      , "comments": this.comments
    }});
    
    Session.set('message', { type: 'alert', severity: 0, priority: 0, ttl: 6000, body: 'Saved to default snapshot', class: 'left no-border done'} );
  },  
  "click .remove": function(event, template) {

    // cannot orphan
    if (this.childs == null || this.childs.length == 0 ) {
      // clean parent
      if (this.parent) {
        Items.update({_id:parent.id}, { $pull: { childs: this._id }});
      }
      // delete item
      Items.remove({_id:this._id});
      $('.modal').fadeOut('fast');
      
    } else {
      // remove item from all ancestors
      Items.update({ ancestors: this._id }, { $pull: { ancestors: this._id }});
    }
    
  }
});

Template.editItem.events({
  "change #name": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "name":template.find("#name").value }})
  },
  "change #description": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "description":template.find("#description").value }})
  },
  "change #project": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "project":template.find("#project").value }})
  },
  "change #type": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "type":template.find("#type").value }})
  },
  "change #status": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "status":template.find("#status").value }})
  },
  "change #state": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "state":template.find("#state").value }})
  },
  "change #due": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "due":template.find("#due").value }})
  },
  "change #estimate": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "estimate":template.find("#estimate").value }})
  },
  "change #effort": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "effort":template.find("#effort").value }})
  },
  "change #score": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "score":template.find("#score").value }})
  },
  "change #owner": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "owner": template.find("#owner").value.split(',') }})
  },
  "change #users": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "users": template.find("#users").value.split(',') }})
  },
  "change #created": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "created":template.find("#created").value }})
  },
  "change #labels": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "labels":template.find("#labels").value }})
  },
  "change #comments": function(event, template) {
    Items.update({_id:this._id}, {$set:{ "comments":template.find("#comments").value }})
  },
  "click .remove": function(event, template) {

    // cannot orphan
    if (this.childs == null || this.childs.length == 0 ) {
      // clean parent
      if (this.parent) {
        Items.update({_id:parent.id}, { $pull: { childs: this._id }});
      }
      // delete item
      Items.remove({_id:this._id});
      $('.modal').fadeOut('fast');
      
    } else {
      // remove item from all ancestors
      Items.update({ ancestors: this._id }, { $pull: { ancestors: this._id }});
    }
    
  },
});

Template.editItem.onCreated(function(){
  console.log('editItem created');
});

Template.editItem.onRendered(function(){
  Meteor.defer(function() {
    tags = this.owner || [];
    $('#owner').tagEditor('destroy');
    $('#owner').tagEditor({ clickDelete: true, initialTags: tags, placeholder: 'Owner...', onChange: function(){ $('#owner').trigger('change'); }}); 

    tags = this.users || [];
    $('#users').tagEditor('destroy');
    $('#users').tagEditor({ clickDelete: true, initialTags: tags, placeholder: 'Assigned to ...', onChange: function(){ $('#users').trigger('change'); }});
  });    
});

Template.card.onRendered(function(){
  
  Meteor.defer(function() {
    $(".draggable").sortable({
      connectWith: ".lane",
      receive: function (event, ui) {
        id = ui.item.context.children[0].id;
        state = this.id
        Items.update(id, { $set: {status: state}});
      }
    });
    
    // DROP CARD ON SIBLING
    $(".droppable").droppable({
      hoverClass: "on-blue",
      drop: function(e, ui) {
       
        // maintains the nested cards list of ancestors
        dz = e.target.id
        newpid = Items.findOne({"_id":dz});
        parent = newpid.parent;
        
        ancestors = newpid.ancestors;
        if (ancestors) {
            ancestors.push(dz)
            ancestors = _.unique(ancestors);
        } else {
            ancestors = [dz]
        }
        
        id = e.toElement.id || ui.draggable[0].childNodes[1].id;
        if (id) {
          children = newpid.childs;
          if (children) {
              children.push(id)
              children = _.unique(children);
          } else {
              children = [id]
          }           
          Items.update({_id:id}, { $set: { parent: dz, ancestors: ancestors }});
          Items.update({_id:dz}, { $set: { childs: children }});
          Items.update({_id:parent}, { $pull: { childs: id }});
        }
      }
    });

    // DROP CARD ON PARENT
    $('.parent').droppable({
      hoverClass: "large on-blue",
      drop: function(e, ui) {
        
        var dragID = ui.draggable.attr('id');
        id = e.toElement.id || ui.draggable.attr('id') || ui.draggable[0].childNodes[1].id;

        var dropID = event.target.id || $(this).attr('id') || this.id;
        
        if (id) {

          // current parent
          parent = Session.get('parent'); // should be the same as dropID
          if (parent) {            
            // SAVE - set or unset at top level
            if (parent.parent) {
              Items.update({_id:id }, { $set: { parent: parent.parent }}, { $pull: { ancestors: parent.id }});
            } else {
              Items.update({_id:id}, { $unset: { parent: "", ancestors: "" }});
            }
            Items.update({_id:parent.id}, { $pull: { childs: id }});
          } else {
            console.log("Top most level bro");
          }

        } else {
          console.log('No id on that badboy - wat?');
        }
      }
    });

  });
  
});

function getList(list, kind) {
  
  entries = Lists.find({ kind : kind }, { sort: { rank: -1} }).fetch();
  extra=entries.map((function(item) {return item.title;}));
  vrbl = kind.toUpperCase() + 'S'
  Session.set(vrbl, _.unique(list.concat(extra).sort()));
  
  return Session.get(vrbl);
}

function getFilters(search) {

  var parent = Session.get("parent");
  if (parent === null) {
    search.$or = [ { parent: null }, { parent: { $exists : false } } ];
  } else {
    search.parent = parent.id;
  }
  
  var project = Session.get("project");
  if (project != "All") {
    search.project = project;
  }

  var label = Session.get("label");
  if (label != "All") {
    search.labels = { $in: [label] }
  }

  var type = Session.get("type");
  if (type != "All") {
    search.type = type
  }
  return search;
  
}

function initFormWith(item) {

  tags = item.owner || [];
  $('#owner').tagEditor('destroy');
  $('#owner').tagEditor({ clickDelete: true, initialTags: tags, placeholder: 'Owner...', onChange: function(){ $('#owner').trigger('change'); }}); 

  tags = item.users || [];
  $('#users').tagEditor('destroy');
  $('#users').tagEditor({ clickDelete: true, initialTags: tags, placeholder: 'Assigned to ...', onChange: function(){ $('#users').trigger('change'); }});

  return true;
}
// poker
function fibonacci(n) {
  var i, sequence = [1, 1]; // [1, 1]
  for (i = 1; i < n; i++)
    sequence.push(sequence[i] + sequence[i - 1]);
    return sequence.splice(1);
}

function chocBox(selection) {
  return selection[Math.floor(Math.random() * selection.length)]
}

