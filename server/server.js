const SITE = 'anodyne-solutions.com'
const DONE = ["Done","Production Ready","Closed","Passed Testing"]
const TODO = ["Ready For Testing","In Progress","To Do", "Open"]
const TEST = ["In Testing","UAT"] 
const STUS = { 'Status' : { 'todo' : TODO, 'test' : TEST, 'done' : DONE } }
const DAYS = 4

Items = new Meteor.Collection('items');
Issues = new Meteor.Collection('atlass');
Lists = new Meteor.Collection('lists');  // is this a thing now?  yes, it is.
Defaults = new Meteor.Collection('defaults');

Meteor.publish('items', function () {
    return Items.find();
});

Meteor.publish('issues', function () {
    return Issues.find();
});

Meteor.publish('lists', function () {
    return Lists.find();
});

Meteor.publish('defaults', function () {
    return Defaults.find();
});

function getItems(itemType, startAt, maxResults) {

    var url = ["https://anodyne.tpondemand.com/api/v1/", itemType,"/?include=[Name,Description,Project,EntityType,EntityState,AssignedUser]&skip=", startAt, "&take=", maxResults].join("");
    var headers = {'Accept' : 'application/json', 'Content-Type' : 'application/json' }
    var secrets =  "admin:P455w0rd."
    
    console.log(url);

    Meteor.http.get(url, { auth: secrets, headers: headers}, function(error, results) {
      
        if (error) { console.log(error) };
        
        console.log('Id,Status,State,Name,Type,Project');
        
        data = results.data.Items
        _.each(data, function(item) {
            createItem(item);
        });
    });
}

function createItem (item) {

    var state, status, type, project, labels
    
    // STATUS
    var state = item.EntityState.Name
    if (TODO.indexOf(state) > -1) {
        status = "todo";
    } else if (TEST.indexOf(state) > -1) {
        status = "test";
    } else {
        status = "done";
    }

    status = chocBox(['todo','test','done','none'])
    project = item.Project.Name
    labels = chocBox([["Front End", "UX"],["Migration"],["Integration"],null])
    
    type = item.EntityType.Name
    if (type == "UserStory") type = "Story"
    type = chocBox([type, type, 'Bug','Story','Task','Test','Impediment','Unicorn'])
    
    // DATA
    var item_id = Items.insert({
        name: item.Name
      , title: item.Id
      , description: item.Description
      , project: project
      , type: type 
      , status: status
      , state: state
      , users: item.AssignedUser.Items
      , labels: labels
      , parent: null // i am top
    });

    // OUTPUT
    console.log(item.Id+','+status+','+state+','+item.Name+','+type+','+project);

}

function mergeIssue (item) {

    var state, status, type, project, labels
    
    // STATUS
    var state = item.Status
    if (TODO.indexOf(state) > -1) {
        status = "todo";
    } else if (TEST.indexOf(state) > -1) {
        status = "test";
    } else {
        status = "done";
    }

    project = item.Project
    
    labels = chocBox([["Front End", "UX"],["Migration"],["Integration"],null]);
    
    type = item.IssueType
    if (type == "UserStory") type = "Story"
    type = chocBox([type, type, 'Bug','Story','Task','Test','Impediment','Unicorn']);
    
    // DATA
    var item_id = Items.insert({
        name: item.Summary
      , title: item.Id
      , description: item.Description
      , project: 'Sharepoint'
      , type: type 
      , status: status
      , state: state
      , users: [ {"Assignee" : item.Assignee, "Reporter" : item.Reporter} ]
      , labels: labels
      , issues: null 
    });

    // OUTPUT
    console.log(item.Id+','+status+','+state+','+item.Name+','+type+','+project);

}

function chocBox(selection) {
  return selection[Math.floor(Math.random() * selection.length)]
}


// Server triggers
Accounts.onCreateUser(function(options, user) {

  var domain = user.emails[0].address.split('@')[1];
  if ( domain.toLowerCase() == SITE) {
    
    var userProperties = {
      profile: options.profile || {},
      isAdmin: false,
    };
    user = _.extend(user, userProperties);

    if (options.email) {
      user.profile.email = options.email;
    }
    
    if (!user.profile.name) {
      user.profile.name = user.username;
    }
    
    if (!Meteor.users.find().count() ) {
      user.isAdmin = true;
    }
  
    if (options.profile) {
      user.profile = options.profile;
    }

    user.nickname = user.emails[0].address.split('@')[0];
    return user;
        
  } else {
    throw "Unauthorized email: staff only!";
  }
  
});

// aaronsw 
Items.after.update(function (userId, doc, fieldNames, modifier, options) {

  if (fieldNames != 'clicks,rank') {
    var rank = Math.round(Math.abs( ((new Date(2013, 1, 17)).getTime() - (new Date()).getTime())/(24*60*60*1000) )) * (DAYS + (doc.clicks||1) + 1);
    console.log(doc._id+' '+fieldNames+' '+rank);
    Items.update({ _id: doc._id}, { $inc : { clicks : 1}, $set: { rank: rank }});
  }

});

// Services
Meteor.methods({  
    'version': function(){
        console.log("OM version 0.10");
    },
    'createNewUser': function (username, email) {
      var userId = Accounts.createUser({username: username, email: email, password: 'Welcome1'});
      Accounts.sendEnrollmentEmail(userId);
    },
    'removeAllItems': function(){
        console.log("Goodbye cruel world");
        return Items.remove({});
    },
    'loadData': function () {
        this.unblock();
        
        Meteor.call("removeAllItems");
        
        getItems('Improvement', 1, 500);
        getItems('Bugs', 1, 500);
        getItems('Documentation', 1, 500);

    },
    'importItems': function () {
      console.log('title,name,description,project,type,status,state,users,labels,issues,docs');
      data = Assets.getText("hotham.json");
      console.log(data);
      var items = JSON.parse(data); 
      
      _.each(items, function(item) {

        console.log(
          item.title +','+
          item.name +','+
          item.description +','+
          item.project +','+
          item.type +','+
          item.status +','+
          item.state +','+
          item.owner +','+
          item.users +','+
          item.labels +','+
          item.comments +','+
          item.docs
        );
        
      });
    },
    'exportItems': function () {
        items = Items.find().fetch();
        
        console.log('title,name,description,project,type,status,state,users,labels,issues,docs');
        
        _.each(items, function(item) {

          console.log(
            item.title +','+
            item.name +','+
            item.description +','+
            item.project +','+
            item.type +','+
            item.status +','+
            item.state +','+
            item.owner +','+
            item.users +','+
            item.labels +','+
            item.comments +','+
            item.docs
          );
          
        });
    },
    'itemList': function (kind) {

        if (kind == null) {
          Lists.remove();
          return 
        }

        Lists.remove({ kind: kind });
        
        var pipeline = [{
            $group: {
                _id: "$"+kind,
                childId: { $first: "$_id" }
            }
        }]
        
        childIds = Items.aggregate(pipeline).map(function(child) { return child.childId });
        
        items = Items.find({_id: {$in: childIds}}).fetch();
        items = _.unique(items);
        
        _(items).each(function(item) {
            value = item[kind]
            var list_id = Lists.insert({kind: kind, title: value});
        });

    },
   'mergeIssues': function () {
        data = Issues.find().fetch();
        items = data[0].Items
        _.each(items, function(item) {
            mergeIssue(item);
        });        
    },
    'addItem': function () {
      var userId = Accounts.createUser({username: username, email: email, password: 'Welcome1'});
      Accounts.sendEnrollmentEmail(userId);
    },
  'countItems': function( search ) {
    return Items.find({ search }).count();
  }
});


// Boot
Meteor.startup(function () {

  Lists.remove({});
  
  Meteor.call("itemList", "project");
  Meteor.call("itemList", "labels");  
  Meteor.call("itemList", "type");  
  
});

